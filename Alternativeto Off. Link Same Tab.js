// ==UserScript==
// @name Alternativeto Official Link Same Tab
// @namespace https://codeberg.org/KuroiLight/Userscripts
// @match https://alternativeto.net/software/*/about/
// @grant none
// @version 1.21
// @description Changes the Official Link for software pages to open in the same tab instead of a new one.
// @run-at document-start
// ==/UserScript==

document.addEventListener('DOMContentLoaded', function() {
    // Find the first 'h3' element with the text 'Official Links'
    let h3Elements = document.querySelectorAll("h3");
    let targetElement = null;
    for (let i = 0; i < h3Elements.length; i++) {
        if (h3Elements[i].textContent === "Official Links") {
            targetElement = h3Elements[i];
            break;
        }
    }

    // Check if the heading was found
    if (targetElement) {
        // Get the parent div
        let parentDiv = targetElement.parentElement;

        // Check if the parent is a div
        if (parentDiv && parentDiv.nodeName === "DIV") {
            // Set the target of all 'a' elements inside the div to '_self'
            let links = parentDiv.querySelectorAll("a");
            links.forEach(link => {
                link.target = "_self";
            });
        } else {
            // The parent is not a div, log an error
            console.error("The parent of the heading is not a div.");
        }
    } else {
        // The heading was not found, log an error
        console.error("The heading 'Official Links' was not found.");
    }
});